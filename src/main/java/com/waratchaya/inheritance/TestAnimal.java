/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.inheritance;

/**
 *
 * @author Melon
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang" , "Black&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero" , "Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom" , "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Dog to = new Dog("To" , "Light Brown");
        to.speak();
        to.walk();
        
        Dog mome = new Dog("Mome" , "Black&White");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("Bat" , "Black&White");
        bat.speak();
        bat.walk();
        
        Duck gabgab = new Duck("GabGab" , "Black&Orange");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        System.out.println("Zom is Animal: " + (zom instanceof Animal));
        System.out.println("Zom is Duck: " + (zom instanceof Duck));
        System.out.println("Zom is Cat: " + (zom instanceof Object));
        
        System.out.println("Dang is Animal: " + (dang instanceof Animal));
        System.out.println("Dang is Dog: " + (dang instanceof Dog));
        System.out.println("Dang is Duck: " + (dang instanceof Object));
        
        System.out.println("Zero is Animal: " + (zero instanceof Animal));
        System.out.println("Zero is Cat: " + (zero instanceof Cat));
        System.out.println("Zero is Dog: " + (zero instanceof Object));
        
        System.out.println("To is Animal: " + (to instanceof Animal));
        System.out.println("To is Dog: " + (to instanceof Dog));
        System.out.println("To is Duck: " + (to instanceof Object));
        
        System.out.println("Mome is Animal: " + (mome instanceof Animal));
        System.out.println("Mome is Dog: " + (mome instanceof Dog));
        System.out.println("Mome is Cat: " + (mome instanceof Object));
        
        System.out.println("Bat is Animal: " + (bat instanceof Animal));
        System.out.println("Bat is Dog: " + (bat instanceof Dog));
        System.out.println("Bat is Duck: " + (bat instanceof Object));
        
        System.out.println("GabGab is Animal: " +(gabgab instanceof Animal));
        System.out.println("GabGab is Duck: " + (gabgab instanceof Duck));
        System.out.println("GabGab is Dog: "+ (gabgab instanceof Object));
        
        System.out.println("Animal is Duck: " + (animal instanceof Duck));
        System.out.println("Animal is Cat: " + (animal instanceof Cat));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;
        
        System.out.println("Ani1: zom is Duck " + (ani1 instanceof Duck));
        
        Animal[] animals = {dang, zero, zom, to, mome, bat, gabgab};
    for(int i=0; i< animals.length; i++) {
        animals[i].walk();
        animals[i].speak();
        if(animals[i] instanceof Duck) {
            Duck duck = (Duck)animals[i];
            duck.fly();
        } 
}
    }
}
